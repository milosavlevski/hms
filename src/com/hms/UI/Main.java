package com.hms.UI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class Main extends JFrame {
    Main() {
        this.setTitle("Hotel Management System");
        initComponents();
    }

    private void initComponents() {
        panel = new JPanel();
        mainMenuBar = new JMenuBar();
        menuItemClients = new JMenuItem();
        menuItemRooms = new JMenuItem();
        menuItemReservations = new JMenuItem();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        GroupLayout panel1Layout = new GroupLayout(panel);
        panel.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
                panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGap(0, 718, Short.MAX_VALUE)
        );
        panel1Layout.setVerticalGroup(
                panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGap(0, 541, Short.MAX_VALUE)
        );

        Font menuFont = new Font("Arial", Font.BOLD, 24);

        menuItemClients.setText("Clients");
        menuItemClients.setFont(menuFont);
        menuItemClients.addActionListener(this::onMenuItemClients);
        mainMenuBar.add(menuItemClients);

        menuItemRooms.setText("Rooms");
        menuItemRooms.setFont(menuFont);
        menuItemRooms.addActionListener(this::onMenuItemRooms);
        mainMenuBar.add(menuItemRooms);

        menuItemReservations.setText("Reservations");
        menuItemReservations.setFont(menuFont);
        menuItemReservations.addActionListener(this::onMenuItemReservations);
        mainMenuBar.add(menuItemReservations);

        setJMenuBar(mainMenuBar);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }

    private void onMenuItemClients(ActionEvent evt) {
        ManageClients clientsForm = new ManageClients();
        clientsForm.pack();
        clientsForm.setLocationRelativeTo(null);
        clientsForm.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        clientsForm.setVisible(true);
    }

    private void onMenuItemRooms(ActionEvent evt) {
        ManageRooms roomsForm = new ManageRooms();
        roomsForm.pack();
        roomsForm.setLocationRelativeTo(null);
        roomsForm.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        roomsForm.setVisible(true);
    }

    private void onMenuItemReservations(ActionEvent evt) {
        ManageReservations reservationsForm = new ManageReservations();
        reservationsForm.pack();
        reservationsForm.setLocationRelativeTo(null);
        reservationsForm.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        reservationsForm.setVisible(true);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> new Main().setVisible(true));
    }

    private JMenuBar mainMenuBar;
    private JMenuItem menuItemClients;
    private JMenuItem menuItemReservations;
    private JMenuItem menuItemRooms;
    private JPanel panel;
}
