package com.hms.UI;

import com.hms.Connection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Login extends JFrame {
    private Login() {
        this.setTitle("Login to HMS");
        initComponents();
        this.setLocationRelativeTo(null);
    }

    private void initComponents() {
        panel = new JPanel();
        panelTitle = new JPanel();
        usernameLabel = new JLabel();
        passwordLabel = new JLabel();
        loginButton = new JButton();
        usernameField = new JTextField();
        passswordField = new JPasswordField();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        usernameLabel.setText("Username:");
        passwordLabel.setText("Password:");

        loginButton.setText("Login");

        loginButton.addActionListener(this::onLoginButton);

        GroupLayout panelLayout = new GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
                panelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(panelTitle, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(panelLayout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addGroup(panelLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                        .addGroup(panelLayout.createSequentialGroup()
                                                .addComponent(passwordLabel)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(passswordField))
                                        .addGroup(panelLayout.createSequentialGroup()
                                                .addComponent(usernameLabel)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(usernameField, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE))
                                        .addComponent(loginButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap(30, Short.MAX_VALUE))
        );
        panelLayout.setVerticalGroup(
                panelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(panelLayout.createSequentialGroup()
                                .addComponent(panelTitle)
                                .addGap(60, 60, 60)
                                .addGroup(panelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(usernameLabel)
                                        .addComponent(usernameField))
                                .addGap(20, 20, 20)
                                .addGroup(panelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(passwordLabel)
                                        .addComponent(passswordField))
                                .addGap(40, 40, 40)
                                .addComponent(loginButton)
                                .addGap(0, 30, Short.MAX_VALUE))
        );

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(panel)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(panel)
        );

        pack();
    }

    private void onLoginButton(ActionEvent evt) {
        PreparedStatement statement;
        ResultSet resultSet;

        String username = usernameField.getText();
        String password = String.valueOf(passswordField.getPassword());

        if (username.trim().equals("")) {
            JOptionPane.showMessageDialog(rootPane, "Enter Your Username to Login", "Empty Username", JOptionPane.WARNING_MESSAGE);
        } else if (password.trim().equals("")) {
            JOptionPane.showMessageDialog(rootPane, "Enter Your Password to Login", "Empty Password", JOptionPane.WARNING_MESSAGE);
        } else {
            Connection connection = new Connection();
            String selectQuery = "SELECT * FROM `users` WHERE `username`=? AND `password`=?";
            try {

                statement = connection.createConnection().prepareStatement(selectQuery);

                statement.setString(1, username);
                statement.setString(2, password);

                resultSet = statement.executeQuery();

                if (resultSet.next()) {
                    Main main = new Main();
                    main.pack();
                    main.setLocationRelativeTo(null);
                    main.setExtendedState(JFrame.MAXIMIZED_BOTH);
                    main.setVisible(true);

                    this.dispose();
                } else {
                    JOptionPane.showMessageDialog(rootPane, "Wrong UserName Or Password", "Login Error", JOptionPane.WARNING_MESSAGE);
                }

            } catch (SQLException e) {
                Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, e);
            }

        }

    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> new Login().setVisible(true));
    }

    private JButton loginButton;
    private JLabel usernameLabel;
    private JLabel passwordLabel;
    private JPanel panel;
    private JPanel panelTitle;
    private JPasswordField passswordField;
    private JTextField usernameField;
}
