package com.hms.UI;

import com.hms.DAL.Room;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class RoomTypes extends JFrame {
    RoomTypes() {
        this.setTitle("Room Types");
        initComponents();
        Room room = new Room();
        room.fillRoomTypesTable(table);
    }

    private void initComponents() {
        scrollPane = new JScrollPane();
        table = new JTable();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        table.setRowHeight(50);
        table.setModel(new DefaultTableModel(
                new Object[][]{},
                new String[]{"Room Type ID", "Label", "Price"}
        ) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        });
        scrollPane.setViewportView(table);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 375, GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(15, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 471, Short.MAX_VALUE)
                                .addContainerGap())
        );

        pack();
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> new RoomTypes().setVisible(true));
    }

    private JScrollPane scrollPane;
    private JTable table;
}
