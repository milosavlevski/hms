package com.hms.UI;

import com.hms.DAL.Client;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class ManageClients extends JFrame {
    private Client client = new Client();

    ManageClients() {
        this.setTitle("Manage Clients");
        initComponents();
        client.fillClientsTable(table);
    }

    private void initComponents() {
        panel1 = new JPanel();
        panel2 = new JPanel();
        idLabel = new JLabel();
        idField = new JTextField();
        firstNameLabel = new JLabel();
        firstNameField = new JTextField();
        lastNameLabel = new JLabel();
        lastNameField = new JTextField();
        phoneLabel = new JLabel();
        phoneField = new JTextField();
        emailLabel = new JLabel();
        emailField = new JTextField();
        scrollPane = new JScrollPane();
        table = new JTable();
        addClientButton = new JButton();
        editClientButton = new JButton();
        removeClientButton = new JButton();
        clearFieldsButton = new JButton();
        refreshTableDataButton = new JButton();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        idLabel.setText("ID:");
        firstNameLabel.setText("First Name:");
        lastNameLabel.setText("Last Name:");
        phoneLabel.setText("Phone:");
        emailLabel.setText("Email:");

        idField.setEditable(false);

        table.setRowHeight(25);
        table.setModel(new DefaultTableModel(
                               new Object[][]{},
                               new String[]{"ID", "First Name", "Last Name", "Phone", "Email"}
                       ) {
                           public boolean isCellEditable(int row, int column) {
                               return false;
                           }
                       }
        );
        table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                onTableCellClick(evt);
            }
        });
        scrollPane.setViewportView(table);

        addClientButton.setText("Add New Client");
        addClientButton.addActionListener(this::onAddClientButton);

        editClientButton.setText("Edit");
        editClientButton.addActionListener(this::onEditClientButton);

        removeClientButton.setText("Remove");
        removeClientButton.addActionListener(this::onRemoveClientButton);

        clearFieldsButton.setText("Clear Fields");
        clearFieldsButton.addActionListener(this::onClearFieldsButton);

        refreshTableDataButton.setText("Refresh");
        refreshTableDataButton.addActionListener(this::onRefreshTableDataButton);

        GroupLayout panel1Layout = new GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
                panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                        .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                                        .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                                                                .addComponent(emailLabel)
                                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(emailField, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                                                                .addComponent(phoneLabel)
                                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(phoneField, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                                                                .addComponent(lastNameLabel)
                                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(lastNameField, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                                                .addGroup(GroupLayout.Alignment.LEADING, panel1Layout.createSequentialGroup()
                                                                        .addComponent(firstNameLabel)
                                                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(firstNameField, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE))
                                                                .addGroup(panel1Layout.createSequentialGroup()
                                                                        .addComponent(idLabel)
                                                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(idField, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE))))
                                                .addGroup(panel1Layout.createSequentialGroup()
                                                        .addComponent(addClientButton, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
                                                        .addGap(18, 18, 18)
                                                        .addComponent(editClientButton, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                                                        .addGap(18, 18, 18)
                                                        .addComponent(removeClientButton, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)))
                                        .addComponent(clearFieldsButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 50, Short.MAX_VALUE)
                                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                        .addComponent(refreshTableDataButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 655, Short.MAX_VALUE))
                                .addGap(23, 23, 23))
        );
        panel1Layout.setVerticalGroup(
                panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(panel1Layout.createSequentialGroup()
                                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGap(41, 41, 41)
                                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addGroup(panel1Layout.createSequentialGroup()
                                                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                        .addComponent(idLabel)
                                                        .addComponent(idField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGap(18, 18, 18)
                                                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                        .addComponent(firstNameLabel)
                                                        .addComponent(firstNameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGap(18, 18, 18)
                                                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                        .addComponent(lastNameLabel)
                                                        .addComponent(lastNameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGap(18, 18, 18)
                                                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                        .addComponent(phoneLabel)
                                                        .addComponent(phoneField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGap(18, 18, 18)
                                                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                        .addComponent(emailLabel)
                                                        .addComponent(emailField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGap(65, 65, 65)
                                                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(addClientButton, GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                                                        .addComponent(editClientButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(removeClientButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                .addGap(18, 18, 18)
                                                .addComponent(clearFieldsButton, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE))
                                        .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(refreshTableDataButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap())
        );

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }

    private void onAddClientButton(ActionEvent evt) {
        String firstName = firstNameField.getText();
        String lastName = lastNameField.getText();
        String phone = phoneField.getText();
        String email = emailField.getText();

        if (firstName.trim().equals("") || lastName.trim().equals("") || phone.trim().equals("")) {
            JOptionPane.showMessageDialog(rootPane, "Required Fields: First / Last Name and Phone Number", "Empty Fields", JOptionPane.WARNING_MESSAGE);
        } else {

            if (client.addClient(firstName, lastName, phone, email)) {
                JOptionPane.showMessageDialog(rootPane, "New Client Added Successfully", "Add Client", JOptionPane.INFORMATION_MESSAGE);
                this.onRefreshTableDataButton(evt);
            } else {
                JOptionPane.showMessageDialog(rootPane, "Client Not Added", "Add Client Error", JOptionPane.ERROR_MESSAGE);
            }

        }

    }

    private void onTableCellClick(MouseEvent evt) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();

        int selectedRow = table.getSelectedRow();

        idField.setText(model.getValueAt(selectedRow, 0).toString());
        firstNameField.setText(model.getValueAt(selectedRow, 1).toString());
        lastNameField.setText(model.getValueAt(selectedRow, 2).toString());
        phoneField.setText(model.getValueAt(selectedRow, 3).toString());
        emailField.setText(model.getValueAt(selectedRow, 4).toString());
    }

    private void onEditClientButton(ActionEvent evt) {
        int id;
        String firstName = firstNameField.getText();
        String lastName = lastNameField.getText();
        String phone = phoneField.getText();
        String email = emailField.getText();

        if (firstName.trim().equals("") || lastName.trim().equals("") || phone.trim().equals("")) {
            JOptionPane.showMessageDialog(rootPane, "Required Fields: First / Last Name and Phone Number", "Empty Fields", JOptionPane.WARNING_MESSAGE);
        } else {

            try {
                id = Integer.parseInt(idField.getText());

                if (client.editClient(id, firstName, lastName, phone, email)) {
                    JOptionPane.showMessageDialog(rootPane, "Client Data Updated Successfully", "Edit Client", JOptionPane.INFORMATION_MESSAGE);
                    this.onRefreshTableDataButton(evt);
                } else {
                    JOptionPane.showMessageDialog(rootPane, "Client Data Not Updated", "Edit Client Error", JOptionPane.ERROR_MESSAGE);
                }

            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(rootPane, "Enter The Client Id (number)", "Client Id Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void onRemoveClientButton(ActionEvent evt) {
        try {
            int id = Integer.parseInt(idField.getText());

            if (client.removeClient(id)) {
                JOptionPane.showMessageDialog(rootPane, "Client Deleted Successfully", "Remove Client", JOptionPane.INFORMATION_MESSAGE);
                this.onRefreshTableDataButton(evt);
            } else {

                JOptionPane.showMessageDialog(rootPane, "Client Not Deleted", "Remove Client Error", JOptionPane.ERROR_MESSAGE);
            }

        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(rootPane, "Enter The Client Id (number)", "Client Id Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void onClearFieldsButton(ActionEvent evt) {
        idField.setText("");
        firstNameField.setText("");
        lastNameField.setText("");
        phoneField.setText("");
        emailField.setText("");

    }

    private void onRefreshTableDataButton(ActionEvent evt) {
        table.setModel(new DefaultTableModel(null, new Object[]{"ID", "First Name", "Last Name", "Phone", "Email"}));
        client.fillClientsTable(table);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> new ManageClients().setVisible(true));
    }

    private JPanel panel1;
    private JPanel panel2;
    private JButton addClientButton;
    private JButton editClientButton;
    private JButton removeClientButton;
    private JButton clearFieldsButton;
    private JButton refreshTableDataButton;
    private JLabel idLabel;
    private JLabel firstNameLabel;
    private JLabel lastNameLabel;
    private JLabel phoneLabel;
    private JLabel emailLabel;
    private JScrollPane scrollPane;
    private JTable table;
    private JTextField emailField;
    private JTextField firstNameField;
    private JTextField idField;
    private JTextField lastNameField;
    private JTextField phoneField;
}
