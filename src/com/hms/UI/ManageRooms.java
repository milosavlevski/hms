package com.hms.UI;

import com.hms.DAL.Room;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class ManageRooms extends JFrame {
    ManageRooms() {
        this.setTitle("Manage Rooms");
        initComponents();
        room.fillRoomTypesSelectBox(roomTypeSelect);
        room.fillRoomsTable(table);

        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(yesRadioButton);
        buttonGroup.add(noRadioButton);
    }

    private void initComponents() {
        panel1 = new JPanel();
        panel2 = new JPanel();
        roomNumberLabel = new JLabel();
        numberField = new JTextField();
        roomTypeLabel = new JLabel();
        roomPhoneLabel = new JLabel();
        phoneField = new JTextField();
        scrollPane = new JScrollPane();
        table = new JTable();
        addRoomButton = new JButton();
        editRoomButton = new JButton();
        removeRoomButton = new JButton();
        clearFieldsButton = new JButton();
        refreshTableDataButton = new JButton();
        roomTypeSelect = new JComboBox<>();
        showRoomTypesButton = new JButton();
        roomReservedLabel = new JLabel();
        panel3 = new JPanel();
        yesRadioButton = new JRadioButton();
        noRadioButton = new JRadioButton();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(1140, 600));

        roomNumberLabel.setText("Number:");
        roomTypeLabel.setText("Type:");
        roomPhoneLabel.setText("Phone:");

        table.setRowHeight(25);
        table.setModel(new DefaultTableModel(
                               new Object[][]{},
                               new String[]{"Number", "Type", "Phone", "Reserved"}
                       ) {
                           public boolean isCellEditable(int row, int column) {
                               return false;
                           }
                       }
        );
        table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                onTableCellClick(evt);
            }
        });
        scrollPane.setViewportView(table);

        addRoomButton.setText("Add New Room");
        addRoomButton.addActionListener(this::onAddRoomButton);

        editRoomButton.setText("Edit");
        editRoomButton.addActionListener(this::onEditRoomButton);

        removeRoomButton.setText("Remove");
        removeRoomButton.addActionListener(this::onRemoveRoomButton);

        clearFieldsButton.setText("Clear Fields");
        clearFieldsButton.addActionListener(this::onClearFieldsButton);

        refreshTableDataButton.setText("Refresh");
        refreshTableDataButton.addActionListener(this::onRefreshTableDataButton);

        showRoomTypesButton.setText("Show Types");
        showRoomTypesButton.addActionListener(this::onShowRoomTypesButton);

        roomReservedLabel.setText("Reserved:");
        yesRadioButton.setText("Yes");
        noRadioButton.setText("No");

        GroupLayout panel3Layout = new GroupLayout(panel3);
        panel3.setLayout(panel3Layout);
        panel3Layout.setHorizontalGroup(
                panel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(panel3Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(yesRadioButton)
                                .addGap(29, 29, 29)
                                .addComponent(noRadioButton)
                                .addContainerGap(31, Short.MAX_VALUE))
        );
        panel3Layout.setVerticalGroup(
                panel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(panel3Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(panel3Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(yesRadioButton)
                                        .addComponent(noRadioButton))
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        GroupLayout panel1Layout = new GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
                panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                                        .addGroup(panel1Layout.createSequentialGroup()
                                                                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                                                        .addComponent(roomTypeLabel)
                                                                        .addComponent(roomNumberLabel))
                                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                                                        .addComponent(numberField, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(roomTypeSelect, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
                                                                .addGap(21, 21, 21))
                                                        .addGroup(panel1Layout.createSequentialGroup()
                                                                .addComponent(addRoomButton, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(editRoomButton, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(removeRoomButton, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)))
                                                .addComponent(clearFieldsButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                                .addComponent(showRoomTypesButton, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                                                .addGroup(GroupLayout.Alignment.LEADING, panel1Layout.createSequentialGroup()
                                                        .addComponent(roomPhoneLabel)
                                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(phoneField, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(panel1Layout.createSequentialGroup()
                                                .addGap(6, 6, 6)
                                                .addComponent(roomReservedLabel)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                        .addComponent(refreshTableDataButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 655, Short.MAX_VALUE))
                                .addGap(23, 23, 23))
        );
        panel1Layout.setVerticalGroup(
                panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(panel1Layout.createSequentialGroup()
                                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGap(41, 41, 41)
                                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addGroup(panel1Layout.createSequentialGroup()
                                                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                        .addComponent(roomNumberLabel)
                                                        .addComponent(numberField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGap(18, 18, 18)
                                                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                        .addComponent(roomTypeSelect, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(roomTypeLabel)
                                                        .addComponent(showRoomTypesButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                .addGap(18, 18, 18)
                                                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                        .addComponent(roomPhoneLabel)
                                                        .addComponent(phoneField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGap(18, 18, 18)
                                                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                                        .addComponent(panel3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                        .addGroup(panel1Layout.createSequentialGroup()
                                                                .addGap(10, 10, 10)
                                                                .addComponent(roomReservedLabel)))
                                                .addGap(116, 116, 116)
                                                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(addRoomButton, GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                                                        .addComponent(editRoomButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(removeRoomButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                .addGap(18, 18, 18)
                                                .addComponent(clearFieldsButton, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE))
                                        .addGroup(panel1Layout.createSequentialGroup()
                                                .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 360, GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                                                .addComponent(refreshTableDataButton)))
                                .addGap(72, 72, 72))
        );

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }

    private void onTableCellClick(MouseEvent evt) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();

        int selectedRow = table.getSelectedRow();

        numberField.setText(model.getValueAt(selectedRow, 0).toString());
        roomTypeSelect.setSelectedItem(model.getValueAt(selectedRow, 1));
        phoneField.setText(model.getValueAt(selectedRow, 2).toString());

        String isReserved = model.getValueAt(selectedRow, 3).toString();

        if (isReserved.equals("Yes")) {
            yesRadioButton.setSelected(true);
        } else if (isReserved.equals("No")) {
            noRadioButton.setSelected(true);
        }
    }

    private void onAddRoomButton(ActionEvent evt) {


        try {
            int roomNumber = Integer.parseInt(numberField.getText());
            int roomType = Integer.parseInt(this.roomTypeSelect.getSelectedItem().toString());
            String phone = phoneField.getText();

            if (room.addRoom(roomNumber, roomType, phone)) {
                JOptionPane.showMessageDialog(rootPane, "New Room Added Successfully", "Add Room", JOptionPane.INFORMATION_MESSAGE);
                this.onRefreshTableDataButton(evt);
            } else {
                JOptionPane.showMessageDialog(rootPane, "Room NOT Added", "Add Room Error", JOptionPane.ERROR_MESSAGE);
            }

        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(rootPane, "Enter The Room number", "Room Number Error", JOptionPane.ERROR_MESSAGE);
        }

    }

    private void onEditRoomButton(ActionEvent evt) {
        int roomNumber;
        int type = Integer.parseInt(roomTypeSelect.getSelectedItem().toString());
        String phone = phoneField.getText();
        String isReserved = "No";

        if (yesRadioButton.isSelected()) {
            isReserved = "Yes";
        }

        if (phone.trim().equals("")) {
            JOptionPane.showMessageDialog(rootPane, "Enter The Room Phone Number", "Empty Fields", JOptionPane.WARNING_MESSAGE);
        } else {

            try {
                roomNumber = Integer.parseInt(numberField.getText());

                if (room.editRoom(roomNumber, type, phone, isReserved)) {
                    JOptionPane.showMessageDialog(rootPane, "Room Data Updated Successfully", "Edit Room", JOptionPane.INFORMATION_MESSAGE);
                    this.onRefreshTableDataButton(evt);
                } else {
                    JOptionPane.showMessageDialog(rootPane, "Room Data Not Updated", "Edit Room Error", JOptionPane.ERROR_MESSAGE);
                }

            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(rootPane, "Enter The Room number", "Room Number Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void onRemoveRoomButton(ActionEvent evt) {
        try {
            int roomNumber = Integer.parseInt(numberField.getText());

            if (room.removeRoom(roomNumber)) {
                JOptionPane.showMessageDialog(rootPane, "Room Deleted Successfully", "Remove Room", JOptionPane.INFORMATION_MESSAGE);
                this.onRefreshTableDataButton(evt);
            } else {
                JOptionPane.showMessageDialog(rootPane, "Room Not Deleted", "Remove Room Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(rootPane, "Enter The Room Number", "Room Number Error", JOptionPane.ERROR_MESSAGE);
        }

    }

    private void onClearFieldsButton(ActionEvent evt) {
        numberField.setText("");
        phoneField.setText("");
        roomTypeSelect.setSelectedIndex(0);
        noRadioButton.setSelected(true);
    }

    private void onRefreshTableDataButton(ActionEvent evt) {
        table.setModel(new DefaultTableModel(null, new Object[]{"Number", "Type", "Phone", "Reserved"}));
        room.fillRoomsTable(table);
    }

    private void onShowRoomTypesButton(ActionEvent evt) {
        RoomTypes roomTypesForm = new RoomTypes();
        roomTypesForm.pack();
        roomTypesForm.setLocationRelativeTo(null);
        roomTypesForm.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        roomTypesForm.setVisible(true);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> new ManageRooms().setVisible(true));
    }

    private Room room = new Room();
    private JPanel panel1;
    private JPanel panel2;
    private JPanel panel3;
    private JButton showRoomTypesButton;
    private JButton addRoomButton;
    private JButton editRoomButton;
    private JButton removeRoomButton;
    private JButton refreshTableDataButton;
    private JButton clearFieldsButton;
    private JComboBox<String> roomTypeSelect;
    private JLabel roomNumberLabel;
    private JLabel roomTypeLabel;
    private JLabel roomPhoneLabel;
    private JLabel roomReservedLabel;
    private JRadioButton noRadioButton;
    private JRadioButton yesRadioButton;
    private JScrollPane scrollPane;
    private JTable table;
    private JTextField numberField;
    private JTextField phoneField;
}
