package com.hms.UI;

import com.hms.DAL.Reservation;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ManageReservations extends JFrame {
    private Reservation reservation = new Reservation();

    ManageReservations() {
        this.setTitle("Manage Reservations");
        initComponents();
        reservation.fillReservationsTable(table);
    }

    private void initComponents() {

        panel1 = new JPanel();
        panel2 = new JPanel();
        idLabel = new JLabel();
        idField = new JTextField();
        clientIdLabel = new JLabel();
        clientIdField = new JTextField();
        roomNumberLabel = new JLabel();
        roomNumberField = new JTextField();
        checkInLabel = new JLabel();
        checkOutLabel = new JLabel();
        scrollPane = new JScrollPane();
        table = new JTable();
        addReservationButton = new JButton();
        editReservationButton = new JButton();
        removeReservationButton = new JButton();
        clearFieldsButton = new JButton();
        refreshTableDataButton = new JButton();
        checkInDatePicker = new com.toedter.calendar.JDateChooser();
        checkOutDatePicker = new com.toedter.calendar.JDateChooser();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        idLabel.setText("ID:");
        clientIdLabel.setText("Client ID:");
        roomNumberLabel.setText("Room:");
        checkInLabel.setText("Check-In:");
        checkOutLabel.setText("Check-Out:");

        idField.setEditable(false);

        table.setRowHeight(25);
        table.setModel(new DefaultTableModel(
                               new Object[][]{},
                               new String[]{"Reservation ID", "Client ID", "Room Number", "Check-in", "Check-out"}
                       ) {
                           public boolean isCellEditable(int row, int column) {
                               return false;
                           }
                       }
        );
        table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                onTableCellClick(evt);
            }
        });
        scrollPane.setViewportView(table);

        addReservationButton.setText("Add New Reservation");
        addReservationButton.addActionListener(this::onAddReservationButton);

        editReservationButton.setText("Edit");
        editReservationButton.addActionListener(this::onEditReservationButton);

        removeReservationButton.setText("Remove");
        removeReservationButton.addActionListener(this::onRemoveReservationButton);

        clearFieldsButton.setText("Clear Fields");
        clearFieldsButton.addActionListener(this::onClearFieldsButton);

        refreshTableDataButton.setText("Refresh");
        refreshTableDataButton.addActionListener(this::onRefreshTableDataButton);

        checkInDatePicker.setDateFormatString("dd/MM/yyyy");
        checkOutDatePicker.setDateFormatString("dd/MM/yyyy");

        GroupLayout jPanel1Layout = new GroupLayout(panel1);
        panel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                        .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                                        .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                                                .addGroup(GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                                                        .addComponent(clientIdLabel)
                                                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(clientIdField, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE))
                                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                                        .addComponent(idLabel)
                                                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(idField, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE)))
                                                        .addGroup(GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                                                .addGap(7, 7, 7)
                                                                .addComponent(checkInLabel)
                                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(checkInDatePicker, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                                                                .addGap(126, 126, 126)))
                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                        .addComponent(addReservationButton, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
                                                        .addGap(18, 18, 18)
                                                        .addComponent(editReservationButton, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                                                        .addGap(18, 18, 18)
                                                        .addComponent(removeReservationButton, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                        .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                                        .addComponent(roomNumberLabel)
                                                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(roomNumberField, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE))
                                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                                        .addComponent(checkOutLabel)
                                                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(checkOutDatePicker, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                                                                        .addGap(120, 120, 120)))
                                                        .addGap(6, 6, 6)))
                                        .addComponent(clearFieldsButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                        .addComponent(refreshTableDataButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 655, Short.MAX_VALUE))
                                .addGap(23, 23, 23))
        );
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGap(41, 41, 41)
                                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                        .addComponent(idLabel)
                                                        .addComponent(idField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGap(18, 18, 18)
                                                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                        .addComponent(clientIdLabel)
                                                        .addComponent(clientIdField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGap(18, 18, 18)
                                                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                        .addComponent(roomNumberLabel)
                                                        .addComponent(roomNumberField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGap(18, 18, 18)
                                                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                                        .addComponent(checkInLabel)
                                                        .addComponent(checkInDatePicker, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGap(18, 18, 18)
                                                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                                        .addComponent(checkOutLabel)
                                                        .addComponent(checkOutDatePicker, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGap(68, 68, 68)
                                                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(addReservationButton, GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                                                        .addComponent(editReservationButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(removeReservationButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                .addGap(18, 18, 18)
                                                .addComponent(clearFieldsButton, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE))
                                        .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(refreshTableDataButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap())
        );

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }

    private void onTableCellClick(MouseEvent evt) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();

        int selectedRow = table.getSelectedRow();

        idField.setText(model.getValueAt(selectedRow, 0).toString());
        clientIdField.setText(model.getValueAt(selectedRow, 1).toString());
        roomNumberField.setText(model.getValueAt(selectedRow, 2).toString());

        try {

            Date dateIn = new SimpleDateFormat("yyyy-MM-dd").parse(model.getValueAt(selectedRow, 3).toString());
            checkInDatePicker.setDate(dateIn);

            Date dateOut = new SimpleDateFormat("yyyy-MM-dd").parse(model.getValueAt(selectedRow, 4).toString());
            checkOutDatePicker.setDate(dateOut);

        } catch (ParseException e) {
            Logger.getLogger(ManageReservations.class.getName()).log(Level.SEVERE, null, e);
        }


    }

    private void onAddReservationButton(ActionEvent evt) {

        try {
            int clientId = Integer.parseInt(clientIdField.getText());
            int roomNumber = Integer.parseInt(roomNumberField.getText());
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String checkIn = dateFormat.format(checkInDatePicker.getDate());
            String checkOut = dateFormat.format(checkOutDatePicker.getDate());

            Date checkInDate = dateFormat.parse(checkIn);
            Date checkOutDate = dateFormat.parse(checkOut);
            Date toDayDate = dateFormat.parse(dateFormat.format(new Date()));

            if (!(checkInDate.after(toDayDate) || checkInDate.equals(toDayDate))) {

                JOptionPane.showMessageDialog(rootPane, "The Check-in Date Must Be After Or Equal To Today's Date", "Check-in Date Error", JOptionPane.ERROR_MESSAGE);

            } else if (!(checkOutDate.after(checkInDate) || checkOutDate.equals(checkInDate))) {
                JOptionPane.showMessageDialog(rootPane, "The Check-out Date Must Be After Or Equal To Check-in Date", "Check-out Date Error", JOptionPane.ERROR_MESSAGE);
            } else {
                if (reservation.addReservation(clientId, roomNumber, checkIn, checkOut)) {
                    JOptionPane.showMessageDialog(rootPane, "New Reservation Added Successfully", "Add Reservation", JOptionPane.INFORMATION_MESSAGE);
                    this.onRefreshTableDataButton(evt);
                } else {
                    JOptionPane.showMessageDialog(rootPane, "Reservation NOT Added", "Add Reservation Error", JOptionPane.ERROR_MESSAGE);
                }

            }
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(rootPane, "Enter The Room number + Client ID", "Input Fields Error", JOptionPane.ERROR_MESSAGE);
        } catch (ParseException e) {
            Logger.getLogger(ManageReservations.class.getName()).log(Level.SEVERE, null, e);
        }


    }

    private void onEditReservationButton(ActionEvent evt) {
        try {
            int reservationId = Integer.parseInt(idField.getText());
            int roomNumber = Integer.parseInt(roomNumberField.getText());
            int clientId = Integer.parseInt(clientIdField.getText());

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String checkIn = dateFormat.format(checkInDatePicker.getDate());
            String checkOut = dateFormat.format(checkOutDatePicker.getDate());

            Date checkInDate = dateFormat.parse(checkIn);
            Date checkOutDate = dateFormat.parse(checkOut);
            Date toDayDate = dateFormat.parse(dateFormat.format(new Date()));

            if (!(checkInDate.after(toDayDate) || checkInDate.equals(toDayDate))) {
                JOptionPane.showMessageDialog(rootPane, "The Check-in Date Must Be After Or Equal To Today's Date", "Check-in Date Error", JOptionPane.ERROR_MESSAGE);
            } else if (!(checkOutDate.after(checkInDate) || checkOutDate.equals(checkInDate))) {
                JOptionPane.showMessageDialog(rootPane, "The Date Out Must Be After Or Equal To Check-in Date", "Date OUT Error", JOptionPane.ERROR_MESSAGE);
            } else {
                if (reservation.editReservation(reservationId, clientId, roomNumber, checkIn, checkOut)) {
                    JOptionPane.showMessageDialog(rootPane, "Reservation Information Updated Successfully", "Edit Reservation", JOptionPane.INFORMATION_MESSAGE);
                    this.onRefreshTableDataButton(evt);
                } else {
                    JOptionPane.showMessageDialog(rootPane, "Reservation Information Not Updated", "Edit Reservation Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(rootPane, "Enter The Room number + The Client ID + Reservation ID", "Data Error", JOptionPane.ERROR_MESSAGE);
        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(rootPane, "Select The Check-in and Check-out Dates", "Date Error", JOptionPane.ERROR_MESSAGE);
        } catch (ParseException e) {
            Logger.getLogger(ManageReservations.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    private void onRemoveReservationButton(ActionEvent evt) {


        try {
            int reservationId = Integer.parseInt(idField.getText());

            if (reservation.removeReservation(reservationId)) {
                JOptionPane.showMessageDialog(rootPane, "Reservation Deleted Successfully", "Remove Reservation", JOptionPane.INFORMATION_MESSAGE);
                this.onRefreshTableDataButton(evt);
            } else {
                JOptionPane.showMessageDialog(rootPane, "Reservation Not Deleted", "Remove Reservation Error", JOptionPane.ERROR_MESSAGE);
            }

        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(rootPane, "Enter The Reservation ID", "Reservation ID Error", JOptionPane.ERROR_MESSAGE);
        }

    }

    private void onClearFieldsButton(ActionEvent evt) {
        clientIdField.setText("");
        idField.setText("");
        roomNumberField.setText("");

        checkInDatePicker.setDate(null);
        checkOutDatePicker.setDate(null);
    }

    private void onRefreshTableDataButton(ActionEvent evt) {
        table.setModel(new DefaultTableModel(null, new Object[]{"Reservation ID", "Client ID", "Room Number", "Check-in", "Check-out"}));
        reservation.fillReservationsTable(table);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> new ManageReservations().setVisible(true));
    }

    private JPanel panel1;
    private JPanel panel2;
    private JButton addReservationButton;
    private JButton clearFieldsButton;
    private JButton editReservationButton;
    private JButton removeReservationButton;
    private JButton refreshTableDataButton;
    private com.toedter.calendar.JDateChooser checkInDatePicker;
    private com.toedter.calendar.JDateChooser checkOutDatePicker;
    private JLabel idLabel;
    private JLabel clientIdLabel;
    private JLabel roomNumberLabel;
    private JLabel checkInLabel;
    private JLabel checkOutLabel;
    private JScrollPane scrollPane;
    private JTable table;
    private JTextField clientIdField;
    private JTextField idField;
    private JTextField roomNumberField;
}
