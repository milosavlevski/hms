package com.hms;

import com.mysql.cj.jdbc.MysqlDataSource;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Connection {
    public java.sql.Connection createConnection() {
        java.sql.Connection connection = null;

        MysqlDataSource dataSource = new MysqlDataSource();

        dataSource.setServerName("localhost");
        dataSource.setPortNumber(3306);
        dataSource.setUser("hms-demo");
        dataSource.setPassword("hms-demo");
        dataSource.setDatabaseName("hms");

        try {
            connection = dataSource.getConnection();
        } catch (SQLException e) {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, e);
        }

        return connection;
    }
}
