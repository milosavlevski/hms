package com.hms.DAL;

import com.hms.Connection;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Client {
    private Connection connection = new Connection();

    public boolean addClient(String firstName, String lastName, String phone, String email) {
        PreparedStatement statement;
        String insertQuery = "INSERT INTO `clients`(`first_name`, `last_name`, `phone`, `email`) VALUES (?,?,?,?)";

        try {
            statement = connection.createConnection().prepareStatement(insertQuery);

            statement.setString(1, firstName);
            statement.setString(2, lastName);
            statement.setString(3, phone);
            statement.setString(4, email);

            return (statement.executeUpdate() > 0);
        } catch (SQLException e) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }

    }

    public boolean editClient(int id, String firstName, String lastName, String phone, String email) {
        PreparedStatement statement;
        String updateQuery = "UPDATE `clients` SET `first_name`=?,`last_name`=?,`phone`=?,`email`=? WHERE `id`=?";

        try {
            statement = connection.createConnection().prepareStatement(updateQuery);

            statement.setString(1, firstName);
            statement.setString(2, lastName);
            statement.setString(3, phone);
            statement.setString(4, email);
            statement.setInt(5, id);

            return (statement.executeUpdate() > 0);
        } catch (SQLException e) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }
    }

    public boolean removeClient(int id) {
        PreparedStatement statement;
        String deleteQuery = "DELETE FROM `clients` WHERE `id`=?";

        try {
            statement = connection.createConnection().prepareStatement(deleteQuery);
            statement.setInt(1, id);
            return (statement.executeUpdate() > 0);

        } catch (SQLException e) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }
    }

    public void fillClientsTable(JTable table) {
        PreparedStatement statement;
        ResultSet resultSet;
        String selectQuery = "SELECT * FROM `clients`";

        try {
            statement = connection.createConnection().prepareStatement(selectQuery);
            resultSet = statement.executeQuery();

            DefaultTableModel tableModel = (DefaultTableModel) table.getModel();

            Object[] row;

            while (resultSet.next()) {
                row = new Object[5];
                row[0] = resultSet.getInt(1);
                row[1] = resultSet.getString(2);
                row[2] = resultSet.getString(3);
                row[3] = resultSet.getString(4);
                row[4] = resultSet.getString(5);

                tableModel.addRow(row);
            }

        } catch (SQLException e) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
