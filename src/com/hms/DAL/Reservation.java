package com.hms.DAL;

import com.hms.Connection;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Reservation {
    private Connection connection = new Connection();
    private Room room = new Room();

    public boolean addReservation(int clientId, int roomNumber, String checkIn, String checkOut) {
        PreparedStatement statement;
        String insertQuery = "INSERT INTO `reservations`(`client_id`, `room_number`, `date_in`, `date_out`) VALUES (?,?,?,?)";

        try {
            statement = connection.createConnection().prepareStatement(insertQuery);

            statement.setInt(1, clientId);
            statement.setInt(2, roomNumber);
            statement.setString(3, checkIn);
            statement.setString(4, checkOut);

            if (room.isRoomReserved(roomNumber).equals("No")) {
                if (statement.executeUpdate() > 0) {
                    room.setRoomToReserved(roomNumber, "Yes");
                    return true;
                } else {
                    return false;
                }
            } else {
                JOptionPane.showMessageDialog(null, "This Room Is Already Reserved", "Room Reserved", JOptionPane.WARNING_MESSAGE);
                return false;
            }


        } catch (SQLException e) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }
    }

    public boolean editReservation(int reservationId, int clientId, int roomNumber, String checkIn, String checkOut) {
        PreparedStatement statement;
        String updateQuery = "UPDATE `reservations` SET `client_id`=?,`room_number`=?,`date_in`=?,`date_out`=? WHERE `id`=?";

        try {
            statement = connection.createConnection().prepareStatement(updateQuery);

            statement.setInt(1, clientId);
            statement.setInt(2, roomNumber);
            statement.setString(3, checkIn);
            statement.setString(4, checkOut);
            statement.setInt(5, reservationId);

            return (statement.executeUpdate() > 0);

        } catch (SQLException e) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }
    }

    public boolean removeReservation(int reservationId) {
        PreparedStatement statement;
        String deleteQuery = "DELETE FROM `reservations` WHERE `id`=?";

        try {
            statement = connection.createConnection().prepareStatement(deleteQuery);
            statement.setInt(1, reservationId);
            int roomNumber = getRoomNumberFromReservation(reservationId);

            if (statement.executeUpdate() > 0) {
                room.setRoomToReserved(roomNumber, "No");
                return true;

            } else {
                return false;
            }

        } catch (SQLException e) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }
    }

    private int getRoomNumberFromReservation(int reservationID) {
        PreparedStatement statement;
        ResultSet resultSet;

        String selectQuery = "SELECT `room_number` FROM `reservations` WHERE `id` = ?";

        try {
            statement = connection.createConnection().prepareStatement(selectQuery);
            statement.setInt(1, reservationID);

            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                return 0;
            }
        } catch (SQLException e) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, e);
            return 0;
        }
    }

    public void fillReservationsTable(JTable table) {
        PreparedStatement statement;
        ResultSet resultSet;
        String selectQuery = "SELECT * FROM `reservations`";

        try {
            statement = connection.createConnection().prepareStatement(selectQuery);
            resultSet = statement.executeQuery();
            DefaultTableModel tableModel = (DefaultTableModel) table.getModel();

            Object[] row;

            while (resultSet.next()) {
                row = new Object[5];
                row[0] = resultSet.getInt(1);
                row[1] = resultSet.getInt(2);
                row[2] = resultSet.getInt(3);
                row[3] = resultSet.getString(4);
                row[4] = resultSet.getString(5);

                tableModel.addRow(row);
            }

        } catch (SQLException e) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}