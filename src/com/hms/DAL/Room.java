package com.hms.DAL;

import com.hms.Connection;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Room {
    private Connection connection = new Connection();

    public boolean addRoom(int number, int type, String phone) {
        PreparedStatement statement;
        String insertQuery = "INSERT INTO `rooms`(`r_number`, `type`, `phone`, `reserved`) VALUES (?,?,?,?)";

        try {

            statement = connection.createConnection().prepareStatement(insertQuery);

            statement.setInt(1, number);
            statement.setInt(2, type);
            statement.setString(3, phone);
            statement.setString(4, "No");

            return (statement.executeUpdate() > 0);


        } catch (SQLException e) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }

    }

    public boolean editRoom(int number, int type, String phone, String isReserved) {
        PreparedStatement statement;
        String updateQuery = "UPDATE `rooms` SET `type`=?,`phone`=?,`reserved`=? WHERE `r_number`=?";

        try {
            statement = connection.createConnection().prepareStatement(updateQuery);
            statement.setInt(1, type);
            statement.setString(2, phone);
            statement.setString(3, isReserved);
            statement.setInt(4, number);

            return (statement.executeUpdate() > 0);

        } catch (SQLException e) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }
    }

    public boolean removeRoom(int roomNumber) {
        PreparedStatement statement;
        String deleteQuery = "DELETE FROM `rooms` WHERE `r_number`=?";

        try {
            statement = connection.createConnection().prepareStatement(deleteQuery);
            statement.setInt(1, roomNumber);
            return (statement.executeUpdate() > 0);
        } catch (SQLException e) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }
    }

    public void fillRoomTypesTable(JTable table) {
        PreparedStatement statement;
        ResultSet resultSet;
        String selectQuery = "SELECT * FROM `type`";

        try {
            statement = connection.createConnection().prepareStatement(selectQuery);
            resultSet = statement.executeQuery();

            DefaultTableModel tableModel = (DefaultTableModel) table.getModel();

            Object[] row;

            while (resultSet.next()) {
                row = new Object[3];
                row[0] = resultSet.getInt(1);
                row[1] = resultSet.getString(2);
                row[2] = resultSet.getString(3);

                tableModel.addRow(row);
            }

        } catch (SQLException e) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void fillRoomsTable(JTable table) {
        PreparedStatement statement;
        ResultSet resultSet;
        String selectQuery = "SELECT * FROM `rooms`";

        try {
            statement = connection.createConnection().prepareStatement(selectQuery);
            resultSet = statement.executeQuery();
            DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
            Object[] row;

            while (resultSet.next()) {
                row = new Object[4];
                row[0] = resultSet.getInt(1);
                row[1] = resultSet.getInt(2);
                row[2] = resultSet.getString(3);
                row[3] = resultSet.getString(4);

                tableModel.addRow(row);
            }

        } catch (SQLException e) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void fillRoomTypesSelectBox(JComboBox comboBox) {
        PreparedStatement statement;
        ResultSet resultSet;
        String selectQuery = "SELECT * FROM `type`";

        try {
            statement = connection.createConnection().prepareStatement(selectQuery);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                comboBox.addItem(resultSet.getInt(1));
            }

        } catch (SQLException e) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    void setRoomToReserved(int number, String isReserved) {
        PreparedStatement statement;
        String updateQuery = "UPDATE `rooms` SET `reserved`=? WHERE `r_number`=?";

        try {
            statement = connection.createConnection().prepareStatement(updateQuery);
            statement.setString(1, isReserved);
            statement.setInt(2, number);
            statement.executeUpdate();

        } catch (SQLException e) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    String isRoomReserved(int number) {
        PreparedStatement statement;
        ResultSet resultSet;
        String selectQuery = "SELECT `reserved` FROM `rooms` WHERE `r_number`=?";

        try {
            statement = connection.createConnection().prepareStatement(selectQuery);
            statement.setInt(1, number);

            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return resultSet.getString(1);
            } else {
                return "";
            }

        } catch (SQLException e) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, e);
            return "";
        }
    }
}
